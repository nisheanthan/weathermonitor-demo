import { makeStyles } from '@mui/styles';

export const HomeStyles = makeStyles(() => ({
    poweredBySpan: {
        display: 'block',
    },
    searchBar: {
        width: '80%',
        margin: 'auto',
    },
    flexRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    content: {
        padding: 10,
        margin: 'auto',
        width: '80%',
    }
}));