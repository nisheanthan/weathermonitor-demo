import React, { useEffect, useState } from 'react';
import axios from 'axios';
import * as _ from 'lodash';

import { HomePageView } from '../view/homeView';
import { axiosInstanceApi } from '../../../axios/instance/axiosInstanceAPI';
import { API } from '../../../axios/constants/axiosAPIConstants';
import { City, Country }  from 'country-state-city';

const HomePageController = () => {
    const sourceToken = axios.CancelToken.source();
    const countryOptions = _.uniqBy(Country.getAllCountries(), 'name');

    const [selectedCountry, setSelectedCountry] = useState('');
    const [selectedCity, setSelectedCity] = useState('');

    const [cityOptions, setCityOptions] = useState([]);
    const [selectedCities, setSelectedCities] = useState([]);

    const [loading, setLoading] = useState(false);
    const [weatherDetailList, setWeatherDetailList] = useState([]);

    const handleCountrySelection = (country) => {
        setSelectedCountry(country);
        country ? setCityOptions(_.uniqBy(City.getCitiesOfCountry(country.isoCode), 'name')) : setCityOptions([]);
    }

    const handleCitySelection = (city) => {
        setSelectedCity(city);
        if (city) {
            if (selectedCities.find((existingCity) => existingCity.name === city.name )) {
                alert('Selected City Is Already Added!');
                setSelectedCountry(null);
                setSelectedCity(null);
                return;
            }
            const newCity = {
                id: Math.random().toString(36).substr(2, 9),
                name: city.name,
            };
            setSelectedCities([...selectedCities, newCity]);
        }
        setSelectedCountry(null);
        setSelectedCity(null);
    }

    const handleDeleteCity = (deletedID) => {
        const filteredCities = selectedCities.filter((city) => city.id !== deletedID);
        setSelectedCities(filteredCities);
    }

    useEffect(() => {
        const json= localStorage.getItem('selectedCities');
        const cities = JSON.parse(json);

        if (cities) {
           setSelectedCities(cities);
        } else {
            const defaultSelections = [
                {
                    id: Math.random().toString(36).substr(2, 9),
                    name: 'Colombo',
                },
                {
                    id: Math.random().toString(36).substr(2, 9),
                    name: 'London',
                }
            ];
            setSelectedCities(defaultSelections);
        }
    }, []);

    useEffect(() => {
        const json = JSON.stringify(selectedCities);
        localStorage.setItem('selectedCities', json);

        const getCityWeatherDetails = () => {
            if (selectedCities.length) {
                setLoading(true);
                setWeatherDetailList([]);
                selectedCities.forEach((city) => {
                    axiosInstanceApi({
                        method: 'GET',
                        url: API.GET_CURRENT_WEATHER_CITY,
                        params: {
                            q: city.name
                        },
                        cancelToken: sourceToken.token
                    }).then((res) => {
                        setWeatherDetailList((prev) => [...prev, {id: city.id, data: res.data}]);
                    }).finally(() => {
                        setLoading(false);
                    })
                });
            }
        }

        getCityWeatherDetails();

        return () => {
            sourceToken.cancel();
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedCities]);

    return (
        <HomePageView
            countryOptions={countryOptions}
            cityOptions={cityOptions}
            handleCitySelection={handleCitySelection}
            handleCountrySelection={handleCountrySelection}
            selectedCountry={selectedCountry}
            selectedCity={selectedCity}
            loading={loading} 
            weatherDetailList={weatherDetailList}
            handleDeleteCity={handleDeleteCity}
        />
    )
}

export default HomePageController;
