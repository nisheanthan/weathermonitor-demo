import { Typography } from '@mui/material';
import React from 'react';
import { SearchBar } from '../../../components/search/SearchBar';
import { CardSliderView } from '../../../components/cardSlider/view/CardSliderView';
import { HomeStyles } from '../style/homeStyle';
import { 
  Card, 
  CardContent,
} from '@mui/material';

export const HomePageView = (props) => {
    const { 
        cityOptions,
        handleCitySelection, 
        countryOptions, 
        handleCountrySelection, 
        selectedCountry, 
        selectedCity,
        weatherDetailList,
        loading,
        handleDeleteCity
    } = props;

    const classes = HomeStyles();

    return (
        <div>
            <div className={classes.searchBar}>
                <div className={classes.flexRow}>
                    <Typography>Country: </Typography>
                    <SearchBar 
                        options={countryOptions} 
                        value={selectedCountry} 
                        isCountry={true} 
                        onSelection={handleCountrySelection} 
                        placeholder={'Enter Country...'}
                    />
                </div>
                <div className={classes.flexRow}>
                    <Typography>City: </Typography>
                    <SearchBar 
                        options={cityOptions} 
                        value={selectedCity} 
                        isCountry={false} 
                        onSelection={handleCitySelection} 
                        placeholder={'Enter City...'}
                    />
                </div>
            </div>
            <Card className={classes.content}>
                <CardContent>
                    This App is a demo weather application where you can search and 
                    add city of your choice to monitor it's weather details.
                    <span className={classes.poweredBySpan}>
                        API powered by <a href='https://openweathermap.org/'>openweathermap.org</a>.
                    </span>
                    <span>Note: Requests are limited to 10 per minute</span>
                </CardContent>
            </Card>
            <CardSliderView isLoading={loading} cardList={weatherDetailList} handleDeleteItem={handleDeleteCity}/>
        </div>
    )
}
