import React from 'react';
import { cardSliderStyle } from '../style/CardSliderStyle';
import Slider from 'react-slick';
import LeftArrow from '../../../assets/img/left-arrow.svg';
import RightArrow from '../../../assets/img/right-arrow.svg';
import { 
  Card, 
  CardContent,
  CardActions,
  CardHeader, 
  Container, 
  CssBaseline, 
  Skeleton, 
  Typography, 
  useMediaQuery, 
  useTheme,
  IconButton
} from '@mui/material';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';

export const CardSliderView = (props) => {
  const { isLoading, cardList, handleDeleteItem } = props;
  const classes = cardSliderStyle();

  const theme = useTheme();
  const mobileDevice = useMediaQuery(theme.breakpoints.down('sm'));

  const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
    <img
      src={LeftArrow}
      alt='prevArrow'
      {...props}
      className={mobileDevice ? classes.previousMb : classes.previous}
      style={{ display: 'block' }}
    />
  );

  const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
    <img
      src={RightArrow}
      alt='nextArrow'
      {...props}
      className={mobileDevice ? classes.nextMb : classes.next}
      style={{ display: 'block' }}
    />
  );

  const cardSliderSettings = {
    infinite: false,
    slidesToShow: 6,
    swipeToSlide: true,
    autoplay: true,
    arrows: true,
    speed: 1500,
    prevArrow: <SlickArrowLeft />,
    nextArrow: <SlickArrowRight />,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 5,
          initialSlide: 1,
          arrows: true,
          swipeToSlide: true,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          infinite: false,
          slidesToShow: 4,
          swipeToSlide: true,
          autoplay: true,
          arrows: true,
          speed: 500,
        },
      },
      {
        breakpoint: 990,
        settings: {
          slidesToShow: 4,
          initialSlide: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 870,
        settings: {
          slidesToShow: 4,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          infinite: false,
          slidesToShow: 4,
          swipeToSlide: true,
          autoplay: true,
          arrows: true,
          speed: 500,
        },
      },
      {
        breakpoint: 670,
        settings: {
          slidesToShow: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 540,
        settings: {
          infinite: false,
          slidesToShow: 2,
          swipeToSlide: true,
          autoplay: true,
          arrows: true,
          speed: 500,
        },
      },
      {
        breakpoint: 420,
        settings: {
          slidesToShow: 1,
          arrows: true,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 1,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 280,
        settings: {
          infinite: false,
          slidesToShow: 1,
          swipeToSlide: true,
          autoplay: true,
          arrows: true,
          speed: 500,
        },
      },
    ],
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      {isLoading ? (
        <Skeleton variant='rect' width={'100%'} height={300} />
      ) : cardList.length === 0 ? (
        <Container className={classes.noCityContainer}>
          <Typography
            align='center'
            style={{ color: 'grey' }}
            variant={mobileDevice ? 'h8' : 'h5'}
            gutterBottom
          >
            It looks like there aren't any city selected yet
          </Typography>
          <Typography
            align='center'
            style={{ color: 'grey' }}
            variant={mobileDevice ? 'subtitle2' : 'subtitle1'}
          >
            You can search for city above.
          </Typography>
        </Container>
      ) : (
        <Container className={classes.slickContainer}>
          <Slider {...cardSliderSettings}>
            {cardList.map((city) => (
              <Card key={city.id} className={classes.card}>
                <CardHeader title={city.data.name} subheader={city.data.sys.country}/>
                <CardContent>
                  <Typography className={classes.temp}>{Math.round(city.data.main.temp)} &#8457;</Typography>
                  <Typography>{city.data.weather[0].main} - {city.data.weather[0].description}</Typography>
                </CardContent>
                <CardActions className={classes.cardActions} disableSpacing>
                  <IconButton onClick={() => handleDeleteItem(city.id)}>
                    <DeleteOutlineIcon/>
                  </IconButton>
                </CardActions>
              </Card>
            ))}
          </Slider>
        </Container>
      )}
    </div>
  );
};
