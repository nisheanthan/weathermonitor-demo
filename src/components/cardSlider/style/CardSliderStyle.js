import { makeStyles } from '@mui/styles';

export const cardSliderStyle = makeStyles(() => ({
  root: {
    padding: '10px'
  },
  previous: {
    padding: 5,
    width: 40,
    height: 40,
    cursor: 'pointer',
    border: 'none',
    borderRadius: '50%',
    backgroundColor: '#fff',
    zIndex: '99 !important',
    boxShadow: 'rgb(35 35 35 / 50%) 0px 0px 20px 5px',
    top: '50%',
    '&:hover': {
      backgroundColor: '#fff',
    },
    left: -25,
    position: 'absolute',
    transform: 'translateY(-50%)',
  },
  previousMb: {
    padding: 5,
    width: 40,
    height: 40,
    cursor: 'pointer',
    border: 'none',
    borderRadius: '50%',
    backgroundColor: '#fff',
    zIndex: '99 !important',
    boxShadow: 'rgb(35 35 35 / 50%) 0px 0px 20px 5px',
    top: '50%',
    '&:hover': {
      backgroundColor: '#fff',
    },
    position: 'absolute',
    transform: 'translateY(-50%)',
  },
  next: {
    padding: 5,
    width: 40,
    height: 40,
    cursor: 'pointer',
    border: 'none',
    borderRadius: '50%',
    backgroundColor: '#fff',
    zIndex: '99 !important',
    boxShadow: 'rgb(35 35 35 / 50%) 0px 0px 20px 5px',
    top: '50%',
    right: -25,
    '&:hover': {
      backgroundColor: '#fff',
    },
    position: 'absolute',
    transform: 'translateY(-50%)',
  },
  nextMb: {
    padding: 5,
    width: 40,
    height: 40,
    cursor: 'pointer',
    border: 'none',
    borderRadius: '50%',
    backgroundColor: '#fff',
    zIndex: '99 !important',
    boxShadow: 'rgb(35 35 35 / 50%) 0px 0px 20px 5px',
    top: '50%',
    right: 0,
    '&:hover': {
      backgroundColor: '#fff',
    },
    position: 'absolute',
    transform: 'translateY(-50%)',
  },
  noCityContainer: {
    height: '100%',
    margin: 'auto'
  },
  slickContainer: {
    display: 'block',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  card: {
    margin: '10px',
    minHeight: '280px',
    display: 'flex!important',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  temp: {
    fontSize: '90px',
    padding: '10px',
    border: '1px solid lightgray',
    borderRadius: '12px'
  },
  cardActions: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end'
  }
}));
