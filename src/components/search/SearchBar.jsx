import React from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { Tooltip } from '@mui/material';

export const SearchBar = (props) => {
    const { options, placeholder, onSelection, isCountry } = props;

    return (
        <Autocomplete
            id='size-small-outlined'
            size='small'
            disabled={options.length <= 0}
            options={options}
            placeholder={placeholder}
            style={{padding: '10px', width: '90%'}}
            onChange={(e, v) => onSelection(v)}
            getOptionLabel={(option) => option.name}
            renderOption={(props, option) => (
                <Box 
                    {...props} 
                    component='li' 
                    sx={{ '& > img': { mr: 2, flexShrink: 0 } }} 
                >
                    <img
                        loading='lazy'
                        width='20'
                        src={`https://flagcdn.com/w20/${isCountry ? option.isoCode.toLowerCase() : option.countryCode.toLowerCase()}.png`}
                        srcSet={`https://flagcdn.com/w40/${isCountry ? option.isoCode.toLowerCase() : option.countryCode.toLowerCase()}.png 2x`}
                        alt=''
                    />
                    {option.name}
                </Box>
            )}
            renderInput={(params) => 
                (
                    !isCountry && options.length <= 0 ? (
                        <Tooltip title='Select a country and wait for few seconds'>
                            <TextField {...params} placeholder={placeholder} />
                        </Tooltip>
                    ) : (
                        <TextField {...params} placeholder={placeholder} />
                    )
                )
            }
        />
    )
}