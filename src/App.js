import './App.css';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { HomeView } from './views';

function App() {
  return (
    <div className='App'>
      <header className='App-header'> Weather Monitor </header>
      <HomeView/>
      <footer className='App-footer'> &copy; All Rights Reserved </footer>
    </div>
  );
}

export default App;
