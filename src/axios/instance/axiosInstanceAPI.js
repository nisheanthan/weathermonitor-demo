import axios from 'axios';
import { AppConst } from '../../constants/appConstants';

export const axiosInstanceApi = axios.create({
    // one minute timeout
    timeout: 60000,
});

axiosInstanceApi.interceptors.request.use((request) => {
    request.headers = {
        'x-rapidapi-host': 'community-open-weather-map.p.rapidapi.com',
        'x-rapidapi-key': AppConst.API_KEY
    }

    return request;
});

axiosInstanceApi.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error && !axios.isCancel(error)) {
            alert(error);
            console.log(error);
        }
        return Promise.reject(error);
    }
);
