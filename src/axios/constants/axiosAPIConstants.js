const API_PREFIX = 'https://community-open-weather-map.p.rapidapi.com/';

export const API = {
    GET_CITY_LIST: API_PREFIX + 'find',
    GET_CURRENT_WEATHER_CITY: API_PREFIX + 'weather',
}
